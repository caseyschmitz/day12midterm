/**
 * You need to change this file. 
 * 
 * This class should inherit from AContestant.
 * 
 * This class should set the value of name to be Darth Vader.
 * 
 * When defending against Michelangelo, this class should return HelperStrings.DarthVaderDefendingMichelangelo
 * 
 * When defending against Chuck Norris, this class should return HelperStrings.DarthVaderDefendingChuckNorris
 * @author bricks
 *
 */

/**
 * Darth Vader is a contestant
 * @author Casey
 *
 */
public class DarthVader extends AContestant {
	/** constructor that calls superconstructor and sends it the contestant's name*/
	public DarthVader() {
		super("Darth Vader");
	}
	
	@Override
	public String defendAgainst(IContestant contestant) {
		
		if (contestant instanceof MichelangeloTheNinjaTurtle)	{
			return HelperStrings.DarthVaderDefendingMichelangelo;
		}
		
		if (contestant instanceof ChuckNorris)	{
			return HelperStrings.DarthVaderDefendingChuckNorris;
		}
		
		else
			return null;
	}
	
	

	
}
