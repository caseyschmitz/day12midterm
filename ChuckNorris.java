/**
 * You need to change this file. 
 * 
 * This class should inherit from AContestant.
 * 
 * This class should set the value of name to be Chuck Norris.
 * 
 * When defending against Michelangelo, this class should return HelperStrings.ChuckDefendingMichelangelo
 * 
 * When defending against Darth Vader, this class should return HelperStrings.ChuckDefendingDarthVader
 * @author bricks
 *
 */

/**
 * Chuck Norris is a contestant
 * @author Casey
 *
 */
public class ChuckNorris extends AContestant	{
	/** constructor that calls superclass constructor and sends it contestant's name*/
	public ChuckNorris ()	{
		super("Chuck Norris");
	}

	@Override
	public String defendAgainst(IContestant contestant) {
		
		if (contestant instanceof MichelangeloTheNinjaTurtle)	{
			return HelperStrings.ChuckDefendingMichelangelo;
		}
		
		if (contestant instanceof DarthVader)	{
			return HelperStrings.ChuckDefendingDarthVader;
		}
		
		else
			return null;
	}
}
