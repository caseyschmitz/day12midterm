/**
 * You need to change this file.
 * 
 * A contestant is an abstract class that represents shared functionality among all contestants
 * AContestant should inherit from IContestant
 * 
 * A contestant should have a private member called name of type String.
 * 
 * A contestant should have a constructor with one parameter of type String.
 * The constructor should assign the value of member name to the value of that parameter
 *
 * This class should override toString(). It should simply return the value of the member name.
 * 
 * @author bricks
 *
 */

/**
 * This is the super class that represents a contestant
 * 
 * @author Casey
 *
 */
public abstract class AContestant implements IContestant	{
	/** this is the name of the contestant*/
	private String name;
	/**
	 * this is the constructor for a contestant
	 * @param inName the name received from contestant subclasses
	 */
	public AContestant (String inName){
		name = inName;
	}
	
	@Override
	public String toString()	{
		return name;
	}

}
