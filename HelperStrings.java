/**
 * !Do not change this file!
 * 
 * These are all the strings that should be returned when a character defends against another character. 
 * 
 * See the individual classes for additional help.
 * 
 * @author bricks
 *
 */
public class HelperStrings {
	public static final String ChuckDefendingMichelangelo = "Michelango goes left and then crosses right. Chuck Norris says, 'No one crosses Chuck Norris and lives.' Michelango goes to the hoop and makes an easy lay-up. That was simple.";
	public static final String ChuckDefendingDarthVader = "Darth Vader throws the ball straight up and then waves his hand to make the ball go right in hoop. Boom. Chuck looks furious, but seems hesitant to complain.";
	public static final String DarthVaderDefendingMichelangelo = "Michelangelo goes in for a dunk, but, wow, out comes the lightsaber and down comes the hoop. I think we'll have to call this a tie. Well, the hoop's gone, so we'll have to call it a day.";
	public static final String DarthVaderDefendingChuckNorris = "Chuck Norris tries for the three-pointer. It looks good until Darth Vader waves his hand and then it flies away. Too bad, it looked like a good shot.";
	public static final String MichelangeloDefendingChuck = "Michelango wrestles the ball from Chuck and !Wham! he takes it to the bucket. Looks like this match is going to the man in green.";
	public static final String MichelangeloDefendingDarthVader = "Michelango respectfully steps out of the way as Darth Vader goes for a simple layout. Yep, it's in. Another victory for Darth Vader.";
}
