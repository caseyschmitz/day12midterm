/**
 * You need to change this file. 
 * 
 * This class should inherit from AContestant.
 * 
 * This class should set the value of name to be Michelangelo.
 * 
 * When defending against Chuck Norris, this class should return HelperStrings.MichelangeloDefendingChuck
 * 
 * When defending against Darth Vader, this class should return HelperStrings.MichelangeloDefendingDarthVader
 * @author bricks
 *
 */

/**
 * Michelangelo is a contestant with a very long name that is difficult to spell
 * @author Casey
 *
 */
public class MichelangeloTheNinjaTurtle extends AContestant	{
	/** contructor that calls the superclass constructor to send it the contestant's name*/
	public MichelangeloTheNinjaTurtle ()	{
		super("Michelangelo");
	}
	
	@Override
	public String defendAgainst(IContestant contestant) {
		
		if (contestant instanceof ChuckNorris)	{
			return HelperStrings.MichelangeloDefendingChuck;
		}
		
		if (contestant instanceof DarthVader)	{
			return HelperStrings.MichelangeloDefendingDarthVader;
		}
		
		else
			return null;
	}
}
