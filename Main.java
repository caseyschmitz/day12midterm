/**
 * !Do not change this file!
 * 
 * Expected output:
 
Welcome to the world's most amazing one-on-one basketball tournament!
Today we have competing with us Chuck Norris and Michelangelo and Darth Vader
Okay, that's the bell. Let's get started.

OK, here's a new match up with Chuck Norris on defense and Michelangelo starting with the ball.
Michelango goes left and then crosses right. Chuck Norris says, 'No one crosses Chuck Norris and lives.' Michelango goes to the hoop and makes an easy lay-up. That was simple.

OK, here's a new match up with Chuck Norris on defense and Darth Vader starting with the ball.
Darth Vader throws the ball straight up and then waves his hand to make the ball go right in hoop. Boom. Chuck looks furious, but seems hesitant to complain.

OK, here's a new match up with Michelangelo on defense and Darth Vader starting with the ball.
Michelango respectfully steps out of the way as Darth Vader goes for a simple layout. Yep, it's in. Another victory for Darth Vader.

OK, here's a new match up with Michelangelo on defense and Chuck Norris starting with the ball.
Michelango wrestles the ball from Chuck and !Wham! he takes it to the bucket. Looks like this match is going to the man in green.

OK, here's a new match up with Darth Vader on defense and Chuck Norris starting with the ball.
Chuck Norris tries for the three-pointer. It looks good until Darth Vader waves his hand and then it flies away. Too bad, it looked like a good shot.

OK, here's a new match up with Darth Vader on defense and Michelangelo starting with the ball.
Michelangelo goes in for a dunk, but, wow, out comes the lightsaber and down comes the hoop. I think we'll have to call this a tie. Well, the hoop's gone, so we'll have to call it a day.

As always, amazing! Thanks for joining us. See you next time.

 * @author bricks
 *
 */
public class Main {

	public static void main(String[] args) {
		new Main();
	}
	
	IContestant[] contestants;
	
	public Main()
	{
		
		System.out.println("Welcome to the world's most amazing one-on-one basketball tournament!");
		
		
		contestants = new IContestant[3];
		contestants[0] = new ChuckNorris();
		contestants[1] = new MichelangeloTheNinjaTurtle();
		contestants[2] = new DarthVader();
		
		System.out.println("Today we have competing with us " + contestants[0] + " and " + contestants[1] + " and " + contestants[2]);
		
		System.out.println("Okay, that's the bell. Let's get started.");
		
		compete(contestants[0], contestants[1]);
		
		compete(contestants[0], contestants[2]);
		
		compete(contestants[1], contestants[2]);
		
		compete(contestants[1], contestants[0]);
		
		compete(contestants[2], contestants[0]);
		
		compete(contestants[2], contestants[1]);
		
		System.out.println();
		System.out.println("As always, amazing! Thanks for joining us. See you next time.");
		
		
	}

	private void compete(IContestant onDefense, IContestant onOffense) {
		System.out.println();
		System.out.println("OK, here's a new match up with " + onDefense + " on defense and " + onOffense + " starting with the ball.");
		System.out.println(onDefense.defendAgainst(onOffense));
		
		
	}

}
