/**
 * You should change this file.
 * 
 * IContestant is an interface. It indicates what shared functionality all contestants have to have.
 * 
 * IContestant should indicate that classes that implement it should have a method called defendAgainst.
 * This method should take as a parameter a reference of type IContestant (indicating the player against which this player is defending).
 * 
 * This method should return the results of the match in the form of a String.
 * @author bricks
 *
 */

/**
 * this is an interface that is implemented by AContestant, all contestants should use its methods
 * @author Casey
 *
 */
public interface IContestant {
	
	/**
	 * this method establishes the contestant that is being defended
	 * @param contestant Icontestant type that represents the contestand being defended
	 * @return string taken from HelperStrings
	 */
	public String defendAgainst (IContestant contestant);
	
}
